# VimPy

A simple / minimalist python script that can be called from VIM. Grants quick access to python's DocTest and Profile functionality.

## Usage
Copy vimpy.py to the root of your python project.

Edit the vimpy.py script main_module string to be whatever you want to pass into python for your main run. For example "-i example.py some_arg" will run "python -i example.py some_arg" for interactive mode.

Add any modules you want to include in the doctest suite to the modules array at the top of vimpy.py.

Remove 'example' from the doctest module list if you'd like.

## Recommended VIM Bindings

These bindings will set up auto save and then run the suite in any VIM mode.
Configure it however you want. Place these into your vimrc.

Shortcut for running Doctest on current file in VIM.
```
noremap  <silent> <F5> <ESC> :w <CR> :!python vimpy.py %<CR>
nnoremap <silent> <F5> <ESC> :w <CR> :!python vimpy.py %<CR>
noremap! <silent> <F5> <ESC> :w <CR> :!python vimpy.py %<CR>
```

Shortcut for running Doctest on all files loaded in Modules
```
noremap  <silent> <F6> <ESC> :w <CR> :!python vimpy.py -test<CR>
nnoremap <silent> <F6> <ESC> :w <CR> :!python vimpy.py -test<CR>
noremap! <silent> <F6> <ESC> :w <CR> :!python vimpy.py -test<CR>
```

Shortcut for running your code.
```
noremap  <silent> <F7> <ESC> :w <CR> :!python vimpy.py -run<CR>
nnoremap <silent> <F7> <ESC> :w <CR> :!python vimpy.py -run<CR>
noremap! <silent> <F7> <ESC> :w <CR> :!python vimpy.py -run<CR>
```

Shortcut for profiling your code. Runs profiler on the same command as -run.
```
noremap  <silent> <F8> <ESC> :w <CR> :!python vimpy.py -profile<CR>
nnoremap <silent> <F8> <ESC> :w <CR> :!python vimpy.py -profile<CR>
noremap! <silent> <F8> <ESC> :w <CR> :!python vimpy.py -profile<CR>
```

## Disclaimer
Simple disclaimer: Any contributor to this project takes absolutely no responsibility for any disruption or damage to your code or VIM set up. Use at your own risk.
