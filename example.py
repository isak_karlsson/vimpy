class Example:
    """This is an example for vimpy."""

    def __init__(self):
        """Test to make sure that VimPy works.
        >>> Example() # Example Success
        Example()
        >>> Example() # Example Failure
        Success!
        """
        pass

    def __repr__(self): return "Example()"
    def __str__(self): return "Success!"

def run(args):
    print("This is the example running with args: {}".format(args))
    e = Example()
    print("Initialized: {}".format(e))

if __name__ == "__main__":
    import sys
    run(sys.argv[1:])
