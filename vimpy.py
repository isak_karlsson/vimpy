import sys, doctest, cProfile, pstats
import example

modules = [ example ]
command = "example.run(['argument'])"

def print_break(before = True):
    if before: string = "\n{}"
    else: string = "{}\n"
    print(string.format('='*70))

def test_all():
    """Tests all given modules."""
    print_break()
    print('Testing all modules.')
    for m in modules: test(m)
    print_break(False)
    
def test(module):
    """Tests the given module."""
    print_break()
    print('Testing {}'.format(module))
    options = doctest.ELLIPSIS | doctest.NORMALIZE_WHITESPACE | doctest.REPORT_ONLY_FIRST_FAILURE
    result = doctest.testmod(m = module, optionflags = options)
    print('-'*40)
    print(result)
    print_break(False)
    
def profile():
    """Profiles the program"""
    print_break()
    print('Profiling main: {}'.format(command))
    cProfile.run(command, 'profiler.out')
    p = pstats.Stats('profiler.out')
    p.strip_dirs()
    p.sort_stats('cumtime')
    p.print_stats(10)
    print_break(False)

def run():
    """Runs the program"""
    print_break()
    print('Running main: {}'.format(command))
    exec(command)
    print_break(False)

if __name__ == "__main__":
    if len(sys.argv) >= 2: 
        arg = sys.argv[1]
        if   arg == '-all': test_all()
        elif arg == '-run': run()
        elif arg == '-profile': profile()
        elif arg == '-test' and len(sys.argv) == 3:
            mod = sys.argv[2]
            if '/' in mod: mod = mod.split('/')[-1]
            if '\\' in mod: mod = mod.split('\\')[-1]
            if '.' in mod: mod = mod.split('.')[0]
            if mod in globals(): test(globals()[mod])
            else: print('{} is not a loaded module.'.format(mod))
        else:
            print('Incorrect usage.')
            print('python vimpy.py [ -all | -run | -profile | -test "module.py" ]')
    else:
        print('Incorrect usage.')
        print('python vimpy.py [ -all | -run | -profile | -test "module.py" ]')
